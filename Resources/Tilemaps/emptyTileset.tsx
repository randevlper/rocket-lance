<?xml version="1.0" encoding="UTF-8"?>
<tileset name="emptyTileset" tilewidth="16" tileheight="16" tilecount="48" columns="16">
 <image source="emptyTileset.png" width="256" height="48"/>
 <tile id="7">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="16">
    <properties>
     <property name="unity:tag" value="Through"/>
    </properties>
   </object>
  </objectgroup>
 </tile>
 <tile id="17">
  <objectgroup draworder="index">
   <object id="9" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
</tileset>
