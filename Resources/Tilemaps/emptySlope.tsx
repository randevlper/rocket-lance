<?xml version="1.0" encoding="UTF-8"?>
<tileset name="emptySlope" tilewidth="16" tileheight="16" tilecount="4" columns="2">
 <image source="emptySlope.png" width="32" height="32"/>
 <tile id="0">
  <objectgroup draworder="index">
   <object id="1" x="0" y="16">
    <polygon points="0,0 16,-16 16,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="1">
  <objectgroup draworder="index">
   <object id="1" x="0.0909091" y="0">
    <polyline points="0,0 15.9091,16 -0.0909091,16 -0.0909091,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="2">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0">
    <polygon points="0,0 16,0 16,16"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="3">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0">
    <polyline points="0,0 16,0 0,16 0,0"/>
   </object>
  </objectgroup>
 </tile>
</tileset>
