﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class PixelLight : MonoBehaviour {

    public static List<PixelLight> Lights;

    public float LightRadius = 1;
    public float LightIntensity = 1;
    [Range (0.0f, 1.0f)]
    public float LightFalloff = 1;
    public Color LightColor = Color.white;

    private Material mat;

    // Create a material that holds the rendering values
    void Start() {
        mat = new Material(Shader.Find("Hidden/PixelLight"));
    }

    // Add this light to the global light list
	void OnEnable () {
        if(Lights == null) Lights = new List<PixelLight>();
        Lights.Add(this);
	}

    // Remove this light from the global light list
    void OnDisable () {
        Lights.Remove(this);
	}

    // Add code for light animation here; example would be a sinus that can adjust the light falloff
    void Update () {

        // Send values to the shader
        mat.SetColor("_Color", LightColor);
        mat.SetVector("_Attributes", new Vector4(LightIntensity, LightFalloff, 0, 0)); // There are two open attributes here for extending the shader
    }

    // Render the light
    public void Render(Mesh quad, int layer) {
        // Build transform matrix
        Matrix4x4 trs = Matrix4x4.TRS(transform.position, Quaternion.identity, Vector3.one * LightRadius * 2);

        // Draw mesh on the light layer
        Graphics.DrawMesh(quad, trs, mat, layer);
    }

    // Draw a preview (only in editor)
    #if UNITY_EDITOR
    void OnDrawGizmos() {
        Handles.color = LightColor;
        Handles.DrawWireDisc(transform.position, Vector3.forward, LightRadius);
        Handles.DrawWireDisc(transform.position, Vector3.forward, (1 - LightFalloff) * LightRadius);
    }
    #endif
}
