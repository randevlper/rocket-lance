// Dither algorithm adjusted from http://alex-charlton.com/posts/Dithering_on_the_GPU/
// Matrices are found on https://en.wikipedia.org/wiki/Ordered_dithering

static const int mat_dither_8x8[8][8] = {
	{ 0, 32, 8, 40, 2, 34, 10, 42 },
	{ 48, 16, 56, 24, 50, 18, 58, 26 },
	{ 12, 44, 4, 36, 14, 46, 6, 38 },
	{ 60, 28, 52, 20, 62, 30, 54, 22 },
	{ 3, 35, 11, 43, 1, 33, 9, 41 },
	{ 51, 19, 59, 27, 49, 17, 57, 25 },
	{ 15, 47, 7, 39, 13, 45, 5, 37 },
	{ 63, 31, 55, 23, 61, 29, 53, 21 }
};

static const int mat_dither_4x4[4][4] = {
	{ 0, 12, 3, 15 },
	{ 8, 4, 11, 7 },
	{ 2, 14, 1, 13 },
	{ 10, 6, 9, 5 }
};

static const int mat_dither_2x2[2][2] = {
	{ 0, 3 },
	{ 3, 1 }
};

float dither8x8(float value, float2 screenCoord) {
	int x = int((screenCoord.x * _ScreenParams.x) % 8);
	int y = int((screenCoord.y * _ScreenParams.y) % 8);
	float d = (mat_dither_8x8[x][y] + 1) / 64.0;

	return 1 - ceil(d - value);
}

float dither4x4(float value, float2 screenCoord) {
	int x = int((screenCoord.x * _ScreenParams.x) % 4);
	int y = int((screenCoord.y * _ScreenParams.y) % 4);
	float d = (mat_dither_4x4[x][y] + 1) / 16.0;

	return 1 - ceil(d - value);
}

float dither2x2(float value, float2 screenCoord) {
	int x = int((screenCoord.x * _ScreenParams.x) % 2);
	int y = int((screenCoord.y * _ScreenParams.y) % 2);
	float d = (mat_dither_2x2[x][y] + 1) / 4.0;

	return 1 - ceil(d - value);
}

// Quick dither method for easier global switching between the matrices
float dither(float value, float2 screenCoord) {
	return dither2x2(value, screenCoord);
}