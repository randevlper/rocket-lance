﻿Shader "Hidden/PixelLight" {
	Properties {
		_Color("Color", Color) = (1, 1, 1, 1)
		_Attributes("Attributes", Vector) = (1, 1, 1, 1)
	}
	SubShader {
		Tags { "RenderType"="Opaque" "Queue"="Geometry" }

		Cull Off
		ZWrite Off

		Blend One One

		Pass {
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			struct vertOut {
				float4 position : SV_POSITION;
				float2 texcoord : TEXCOORD0;
			};

			vertOut vert (float4 vertex : POSITION, float2 texcoord : TEXCOORD0) {
				vertOut o;

				o.position = UnityObjectToClipPos(vertex);
				o.texcoord = texcoord;

				return o;
			}
			
			fixed4 _Color;
			fixed4 _Attributes;

			fixed4 frag (vertOut i) : SV_Target {
				fixed falloff = smoothstep(0.0, _Attributes.y, 1 - distance(i.texcoord, 0.5) * 2);
				return _Color * _Attributes.x * falloff;
			}
			ENDCG
		}
	}
}
