﻿Shader "Hidden/PixelLightPost" {
	Properties {
		_MainTex("Screen", 2D) = "white" {}
		_LightTex("Light", 2D) = "black" {}
	}
	SubShader {

		Pass {
			CGPROGRAM
			#pragma vertex vert_img
			#pragma fragment frag

			#include "UnityCG.cginc"

			sampler2D _MainTex;
			sampler2D _LightTex;

			fixed4 frag (v2f_img i) : SV_Target {
				return tex2D(_MainTex, i.uv) * tex2D(_LightTex, i.uv);
			}
			ENDCG
		}
	}
}
