: Okay here are the 4 tiles for the post process. I avoided putting it on one camera in the end but the lightcamera will be generated automaticly by code.
Steps:
1) Add PixelLightCamera your main camera
2) Add a new layer for the lights. This is unavoidable sadly. Put the layername in the string slot ofh the PixelLightCamera script.
3) Add PixelLight components wherever you want a light. There is a rough preview gizmo on them.
4) Adjust the scripts to whatever you actually want to do. You can use the update method on the light component to animate the lights or add it by shader by using the 2 remaining attributes.