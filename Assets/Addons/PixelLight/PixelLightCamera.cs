﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PixelLightCamera : MonoBehaviour {


    //Change this to layer
    public string LightLayer = "PixelLight";
    public Color AmbientColor;

    private Camera cam;
    private Camera lightCam;

    private RenderTexture renderTarget;

    private Mesh quad;

    private Material postMat;

    private int layer;
    private LayerMask layerMask;

    void Start() {
        // Get camera
        cam = GetComponent<Camera>();

        // Set up the post process material
        postMat = new Material(Shader.Find("Hidden/PixelLightPost"));

        // Get layer
        layer = LayerMask.NameToLayer(LightLayer);
        if(layer == -1) {
            Debug.LogError("No layer named " + LightLayer);
            this.enabled = false;
        }
        layerMask = LayerMask.GetMask(LightLayer);

        // Create a new camera for light rendering
        lightCam = new GameObject("PixelLightCamera").AddComponent<Camera>();
        lightCam.CopyFrom(cam);
        lightCam.transform.SetParent(cam.transform);

        lightCam.cullingMask = layerMask;
        lightCam.renderingPath = RenderingPath.Forward;
        lightCam.clearFlags = CameraClearFlags.SolidColor;
        lightCam.allowHDR = false;
        lightCam.allowMSAA = false;

        // Build a simple quad mesh
        quad = new Mesh();

        quad.vertices = new Vector3[] {
            new Vector3(-0.5f, -0.5f, 0.0f),
            new Vector3(-0.5f, 0.5f, 0.0f),
            new Vector3(0.5f, -0.5f, 0.0f),
            new Vector3(0.5f, 0.5f, 0.0f)
        };

        quad.uv = new Vector2[] {
            new Vector2(0, 0),
            new Vector2(0, 1),
            new Vector2(1, 0),
            new Vector2(1, 1)
        };

        quad.triangles = new int[] { 0, 1, 2, 3, 2, 1 };
        quad.RecalculateBounds();
    }

	void Update () {
        // Generate a render target if there is none or the screen resolution changed
		if(!renderTarget || renderTarget.width != Screen.width || renderTarget.height != Screen.height) {
            renderTarget = new RenderTexture(Screen.width, Screen.height, 0, RenderTextureFormat.ARGB32);
            lightCam.targetTexture = renderTarget;
            postMat.SetTexture("_LightTex", renderTarget);
        }

        // Set color for ambient
        lightCam.backgroundColor = AmbientColor;

        // Render lights
        if (PixelLight.Lights != null) {
            for (int i = 0; i < PixelLight.Lights.Count; i++) {
                // Render the current light; Render them one by one since instancing doesn't give performance gains for so few vertices
                PixelLight.Lights[i].Render(quad, layer);
            }
        }

    }

    // Render the post process
    void OnRenderImage(RenderTexture src, RenderTexture dest) {
        Graphics.Blit(src, dest, postMat);
    }

}
