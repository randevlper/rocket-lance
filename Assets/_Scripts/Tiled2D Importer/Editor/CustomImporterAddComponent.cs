﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Tiled2Unity.CustomTiledImporter]
class CustomImporterAddComponent : Tiled2Unity.ICustomTiledImporter
{
    public void HandleCustomProperties(UnityEngine.GameObject gameObject,
        IDictionary<string, string> props)
    {
        // Simply add a component to our GameObject
        if (props.ContainsKey("AddComp"))
        {
            System.Type componentType = System.Type.GetType(props["AddComp"] + ",Assembly-CSharp");
            gameObject.AddComponent(componentType);

            if (componentType.ToString() == "Door")
            {
                Door door = gameObject.GetComponent<Door>();
                door.boxCollider2D = gameObject.GetComponent<BoxCollider2D>();

                if (props.ContainsKey("GoToDoor"))
                {
                    door.GoToDoor = props["GoToDoor"];
                }
                else
                {
                    Debug.LogError("GoToDoor does not exsist!", gameObject);
                }

                if (props.ContainsKey("GoToScene"))
                {
                    door.GoToScene = props["GoToScene"];

                }
                else
                {
                    Debug.LogError("GotoScene does not exsist!", gameObject);
                }

                if (props.ContainsKey("IsFloor"))
                {
                    door.IsFloor = props["IsFloor"] == "True";
                }
                else
                {
                    door.IsFloor = false;
                }

                Area2D area2D = gameObject.AddComponent<Area2D>();
                door.area2D = area2D;
                //This done on import
                area2D.col2D = door.boxCollider2D;
                area2D.mask.layerMask = LayerMask.GetMask("Body", "Player");
                area2D.mask.useLayerMask = true;

            }
        }
    }

    public void CustomizePrefab(GameObject prefab)
    {
        //
        // Do nothing
    }
}