﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BombController : MonoBehaviour, IDamageable
{
    public float radiusOfEffect;
    public float fuseTime;
    public int flashTimes;
    public DamageData damage;
    public IDamageable damageableSelf;
    
    public Rigidbody2D rb2D;
    public CharacterSpriteManager characterSpriteManager;

    Gold.Timer timer;

    private void Awake()
    {
        timer = new Gold.Timer(Explode,fuseTime,false);
        damageableSelf = GetComponent<IDamageable>();
    }


    public void Light()
    {
        timer.Start();
        characterSpriteManager.Flash(flashTimes, fuseTime/flashTimes);
    }

    private void Update()
    {
        timer.Tick(Time.deltaTime);
    }

    public void Explode()
    {
        //Debug.Log("KABOOM");
        rb2D.velocity = Vector2.zero;
        //transform.localScale = new Vector2(radiusOfEffect, radiusOfEffect);
        Collider2D[] hits = Physics2D.OverlapCircleAll(transform.position, radiusOfEffect);
        foreach (Collider2D element in hits)
        {
            IDamageable damageable = element.gameObject.GetComponent<IDamageable>();
            if (damageable != null && damageableSelf != damageable)
            {
                damageable.Damage(damage);
            }
        }
		Remove();
    }

    public void Remove()
    {
        gameObject.SetActive(false);
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, radiusOfEffect);
    }

	public void Damage(DamageData hit)
	{
        Explode();
	}

    public void Setup(Vector2 position, Vector2 velocity, Quaternion rotation)
    {
        transform.position = position;
        rb2D.velocity = velocity;
        transform.localRotation = rotation;
        timer.Reset();
        timer.Stop();
    }

}
