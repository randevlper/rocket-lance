﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelManager
{

    //static public LevelManager instance;
    static public string spawn = "idol_entrance_v2";


    bool _loadingNextArea = false;
    static string sceneFolder = "/_Scenes/";

    //Area loading info
    string _goToDoor;
    bool _isFloor;

    // private void Awake () {
    //     if (instance == null) {
    //         instance = this;
    //         DontDestroyOnLoad (gameObject);
    //     } else if (instance != this) {
    //         Destroy (this);
    //     }
    // }
    
    public LevelManager()
    {
        SceneManager.sceneLoaded += OnSceneFinishedLoading;
    }

    private void OnSceneFinishedLoading(Scene scene, LoadSceneMode mode)
    {
        //Debug.Log ("Level Loaded: ( " + scene.buildIndex + " " + scene.name + " ) " + mode);
        if (_loadingNextArea)
        {
            //Place player here

            GameObject door = GameObject.Find(_goToDoor);
            if (door == null)
            {
                Debug.LogError(_goToDoor + " NOT FOUND!" + " Not moving player!");
            }
            else
            {
                Vector3 doorPosition = door.transform.position;
                //Debug.Log (PlayerController.instance.gameObject.name + " going to " + door);
                PlayerController.instance.gameObject.transform.position = doorPosition;
                CameraController.instance.gameObject.transform.position =
                    new Vector3(doorPosition.x, doorPosition.y,
                        CameraController.instance.gameObject.transform.position.z);

                if(_isFloor)
                {

                }
            }
            _loadingNextArea = false;
        }

    }

    public void LoadNextScene(string sceneName, string doorName, bool isFloor)
    {
        _goToDoor = doorName;
        _isFloor = isFloor;

        _loadingNextArea = true;
        //Disable player controls
        //Start loading scene
        if (DoesSceneExist(sceneName))
        {
            SceneManager.LoadSceneAsync(sceneName);
        }
        else
        {
            Debug.LogError("Scene " + sceneName + " does not exist!");
        }

        //Fade to black
        //Move player to new point
        //Simulate player movement to point out of collider
        //Flag collider to collider with player, Allow Player to input again
    }

    public void Respawn()
    {
        SceneManager.LoadSceneAsync(spawn);
    }

    static bool DoesSceneExist(string name)
    {
        string fileLocation = Application.dataPath + sceneFolder + name.Split('_')[0] + "/" + name + ".unity";
        //Debug.Log(fileLocation);
        return System.IO.File.Exists(fileLocation);
    }

}