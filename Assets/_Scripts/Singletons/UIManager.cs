﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


//This should become a service 
public class UIManager : MonoBehaviour {

	private void Start()
	{
		Setup();
	}

	//Should only be called once for the player
	public void Setup()
	{
		PlayerController.instance.characterHealth.OnHealthChanged += SetHealthBarValue;
		PlayerController.instance.characterHealth.OnMaxHealthChanged += SetHealthBarValue;
	}

	//Player UI
	[SerializeField]
	private Slider healthBar;
	public void SetHealthBarValue (float value) {
		healthBar.normalizedValue = Mathf.Clamp (value, 0.0f, 1.0f);
	}

}