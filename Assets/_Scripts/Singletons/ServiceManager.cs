﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ServiceManager : MonoBehaviour
{
    public static ServiceManager instance;

    public GameManager gameManager;
    public LevelManager levelManager;
    public UIManager uiManager;

    // Use this for initialization
    void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
            gameManager = new GameManager();
            levelManager = new LevelManager();
            
            uiManager = GameObject.Find("_Canvas").GetComponent<UIManager>();
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }
    }
}
