﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using XInputDotNetPure;

namespace Gold
{
}


//Putting this off until its needed for controller support
public class InputManager
{
    struct Axis
    {
        //Positive button
        //Negative button
        public KeyCode positive; //If pressed -1
        public KeyCode negative; //If pressed 1
    }

    struct Button
    {
        public KeyCode keycode;
    }

    static Dictionary<string, Axis> _axis = new Dictionary<string, Axis>();
    static Dictionary<string, Button> _buttons = new Dictionary<string, Button>();

    public InputManager()
    {

    }

    //A list of Buttons
    //A list of Axis

    //Get a button down
    //Get a button up
    //Get button

    // o Return a float, describing the value of the axis named in the parameter.
    // o Throw an exception if the parameter, axisName, does not match known axes.
    public static float GetAxisRaw(string axisName)
    {
        float retval = 0;
        Axis value;
        if (_axis.TryGetValue(axisName, out value))
        {
            if (Input.GetKey(value.positive))
            {
                retval += 1;
            }

            if (Input.GetKey(value.negative))
            {
                retval += -1;
            }
        }

        return retval;
    }

    // o Return a bool, describing whether the button is currently being pressed or not.
    // o Throw an exception if the parameter, buttonName, does not match any known buttons.
    public static bool GetButton(string buttonName)
    {
        Button value;
        if (_buttons.TryGetValue(buttonName, out value))
        {
            return Input.GetKeyDown(value.keycode);
        }

        throw new System.NullReferenceException();
    }

    public static bool GetButtonUp(string buttonName)
    {
        Button value;
        if (_buttons.TryGetValue(buttonName, out value))
        {
            return Input.GetKeyUp(value.keycode);
        }

        throw new System.NullReferenceException();
    }

    public static bool GetButtonDown(string buttonName)
    {
        Button value;
        if (_buttons.TryGetValue(buttonName, out value))
        {
            return Input.GetKeyDown(value.keycode);
        }

        throw new System.NullReferenceException();
    }

    // o Return a bool, describing whether the mouse button is currently being pressed or not.
    public static bool GetMouseButton(int buttonNumber)
    {
        return Input.GetMouseButton(buttonNumber);
    }

    public static bool GetMouseButtonDown(int buttonNumber)
    {
        return Input.GetMouseButtonDown(buttonNumber);
    }

    public static bool GetMouseButtonUp(int buttonNumber)
    {
        return Input.GetMouseButtonUp(buttonNumber);
    }

    // o Return the position of the mouse in screen space.
    public static Vector2 GetMousePosition()
    {
        return Input.mousePosition;
    }

    // o Defines an axis as a set of inputs that move it from -1 to 1.
    // o Throw an exception if an axis by that name already exists.
    public static void AddAxis(string axisName, string negativeInput, string positiveInput)
    {
        Axis newAxis = new Axis();
        try
        {
            newAxis.negative = StringToKeycode(negativeInput);
        }
        catch (System.Exception)
        {
            Debug.LogError("This keycode does not exist!" + negativeInput);
            return;
        }

        try
        {
            newAxis.positive = StringToKeycode(positiveInput);
        }
        catch (System.Exception)
        {
            Debug.LogError("This keycode does not exist!" + positiveInput);
            return;
        }
        AddAxis(axisName, newAxis);
    }

    public static void AddAxis(string axisName, KeyCode negativeInput, KeyCode positiveInput)
    {
        Axis newAxis = new Axis();
        newAxis.negative = negativeInput;
        newAxis.positive = positiveInput;
        AddAxis(axisName, newAxis);
    }

    static void AddAxis(string axisName, Axis axis)
    {
        if (!_axis.ContainsKey(axisName))
        {
            _axis.Add(axisName, axis);
        }
        else
        {
            throw new System.Exception();
        }
    }

    // * void SetAxis(string axisName, string negativeInput, string positiveInput)
    // o Updates an existing axis with a new set of inputs that move it from -1 to 1.
    // o Throw an exception if an axis by that name does not already exist.
    // o Defines a button as an input that is either pressed or not pressed.
    public static void SetAxis(string axisName, string negativeInput, string positiveInput)
    {

    }

    public static void SetAxis(string axisName, KeyCode negativeInput, KeyCode positiveInput)
    {
        
    }

    public static void AddButton(string buttonName, string input)
    {
        Button newButton = new Button();
        try
        {
            newButton.keycode = StringToKeycode(input);
        }
        catch (System.Exception)
        {
            Debug.LogError("This keycode does not exist!" + input);
            return;
        }
        // o Throw an exception if a button by that name already exists.
        AddButton(buttonName, newButton);
    }

    public static void AddButton(string buttonName, KeyCode input)
    {
        Button newButton = new Button();
        newButton.keycode = input;
        // o Throw an exception if a button by that name already exists.
        AddButton(buttonName, newButton);
    }

    static void AddButton(string buttonName, Button button)
    {
        if (!_buttons.ContainsKey(buttonName))
        {
            _buttons.Add(buttonName, button);
        }
        else
        {
            throw new System.Exception();
        }
    }

    // * void SetButton(string buttonName, string input)
    // o Updates an existing button with a new input that is either pressed or not pressed.
    // o Throw an exception if an axis by that name does not already exist.

    public static void SetButton(string buttonName, string input)
    {
        
    }

    public static void SetButton(string buttonName, KeyCode input)
    {

    }

    //All keycodes start with capital letter
    public static KeyCode StringToKeycode(string keyCode)
    {
        return (KeyCode)System.Enum.Parse(typeof(KeyCode), keyCode);
    }

    //Loop through the _axis or _buttons and check if something exsists with the name
}
