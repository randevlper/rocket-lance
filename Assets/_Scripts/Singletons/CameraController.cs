﻿using System.Collections;
//using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {

	public static CameraController instance;

	public GameObject target;
	public float verticalOffset;
	public float lookAheadDstX;
	public float lookSmoothTimeX;
	public float verticalSmoothTime;
	public Vector2 focusAreaSize;

	FocusArea _focusArea;

	Collider2D targetCollider;
	CharacterMotor2D targetMotor2D;

	float currentLookAheadX;
	float targetLookAheadX;
	float lookAheadDirX;
	float smoothLookVelocityX;
	float smoothVelocityY;

	bool lookAheadStopped;

	void Awake () {
		if (instance == null) {
			instance = this;
			DontDestroyOnLoad (gameObject);
			target = GameObject.FindGameObjectWithTag ("Player");
			targetCollider = target.GetComponent<Collider2D> ();
			targetMotor2D = target.GetComponent<CharacterMotor2D> ();
			_focusArea = new FocusArea (targetCollider.bounds, focusAreaSize);
		} else if (instance != this) {
			Destroy (gameObject);
		}
	}

	void Update () {
		_focusArea.Update (targetCollider.bounds);
	}

	void LateUpdate () {
		_focusArea.Update (targetCollider.bounds);
		Vector2 focusPosition = _focusArea.center + Vector2.up * verticalOffset;

		if (_focusArea.velocity.x != 0) {
			lookAheadDirX = Mathf.Sign (_focusArea.velocity.x);
			if (Mathf.Sign (targetMotor2D.characterInput.x) ==
				Mathf.Sign (_focusArea.velocity.x) && targetMotor2D.characterInput.x != 0) {
				lookAheadStopped = false;
				targetLookAheadX = lookAheadDirX * lookAheadDstX;
			} else {
				if (!lookAheadStopped) {
					lookAheadStopped = true;
					targetLookAheadX = currentLookAheadX +
						(lookAheadDirX * lookAheadDstX - currentLookAheadX) / 4f;
				}
			}
		}

		currentLookAheadX = Mathf.SmoothDamp (currentLookAheadX, targetLookAheadX, ref smoothLookVelocityX, lookSmoothTimeX);

		focusPosition.y = Mathf.SmoothDamp (transform.position.y, focusPosition.y, ref smoothVelocityY, verticalSmoothTime);
		focusPosition += Vector2.right * currentLookAheadX;

		transform.position = (Vector3) focusPosition + Vector3.forward * -10;
	}

	void OnDrawGizmos () {
		Gizmos.color = new Color (1, 0, 0, 0.5f);
		Gizmos.DrawCube (_focusArea.center, focusAreaSize);

	}

	struct FocusArea {
		public Vector2 center;
		public Vector2 velocity;
		float left, right;
		float top, bottom;

		public FocusArea (Bounds targetBounds, Vector2 size) {
			left = targetBounds.center.x - size.x / 2;
			right = targetBounds.center.x + size.x / 2;
			bottom = targetBounds.min.y;
			top = targetBounds.max.y + size.y;

			center = new Vector2 ((left + right) / 2, (top + bottom) / 2);
			velocity = new Vector2 ();
		}

		public void Update (Bounds targetBounds) {
			float shiftX = 0;
			if (targetBounds.min.x < left) {
				shiftX = targetBounds.min.x - left;
			} else if (targetBounds.max.x > right) {
				shiftX = targetBounds.max.x - right;
			}
			left += shiftX;
			right += shiftX;

			float shiftY = 0;
			if (targetBounds.min.y < bottom) {
				shiftY = targetBounds.min.y - bottom;
			} else if (targetBounds.max.y > top) {
				shiftY = targetBounds.max.y - top;
			}
			top += shiftY;
			bottom += shiftY;

			center = new Vector2 ((left + right) / 2, (top + bottom) / 2);
			velocity = new Vector2 (shiftX, shiftY);
		}
	}
}