﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Gold;


//Create a base class for enemyAI
public class ThrowerFan : EnemyAI
{
    //Pod racing
    public Vector2 throwVelocity;
    [SerializeField] private GameObject throwPrefab;
    Gold.ObjectPool objectPool;
    public float throwTime;
    Timer throwTimer;

    // Use this for initialization
    protected override void Start()
    {
        base.Start();
        objectPool = new ObjectPool(throwPrefab, 1);

        throwTimer = new Timer(Throw, throwTime, true);
        throwTimer.Start();

		stateMachine.Add(new StateMachineAction(Throwing), "Throwing");
		stateMachine.Start("Throwing");
    }

    void Throwing()
    {
        //Get a bullet from the pool
        //initialization
        //Throw
        throwTimer.Tick(Time.deltaTime);
    }

    Vector2 relativeVelocity;
    void Throw()
    {
        GameObject thrownItem = objectPool.Get();

        if (thrownItem != null)
        {
            thrownItem.SetActive(true);
			thrownItem.transform.position = transform.position;

			BombController bomb = thrownItem.GetComponent<BombController>();
            relativeVelocity = throwVelocity;
            relativeVelocity.x *= character.facingDirection;
			bomb.Setup(transform.position, relativeVelocity, Quaternion.identity);
			bomb.gameObject.SetActive(true);
			bomb.Light();
            // thrownItem.GetComponent<Rigidbody2D>().velocity =
            //     new Vector2(throwVelocity.x * character.facingDirection, throwVelocity.y);
        }
    }

    void Idle()
    {
        character.SetDirectionalInput(0, 0);
    }
}
