﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Gold;

public class EnemyAI : MonoBehaviour
{

    [SerializeField] protected Character character;
    [SerializeField] protected AIBehaviourCanMoveLeft canMoveLeft;
    [SerializeField] protected AIBehaviourCanSeeTarget canSeeTarget;

    [HideInInspector] public GameObject target;

    protected StateMachine stateMachine;
    public float walkSpeed;
    public float sprintSpeed;

    protected virtual void Awake()
    {
        stateMachine = new StateMachine();
    }

    // Use this for initialization
    protected virtual void Start()
    {
        target = PlayerController.instance.gameObject;
        canSeeTarget.target = PlayerController.instance.hitbox;
    }

    // Update is called once per frame
    protected virtual void Update()
    {
        stateMachine.Tick();
    }
}
