﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIBehaviourCanMoveLeft : MonoBehaviour
{
    public Character character;
    public BoxCollider2D boxCollider2D;
    bool canWalkLeft = false;
    public bool isEdgeDetecting;
	float raycastLength = 0.1f;

    // Update is called once per frame
    public bool GetCanWalkLeft()
    {
        if (character.isOnLeftWall())
        {
            canWalkLeft = false;
        }
        if (character.isOnRightWall())
        {
            canWalkLeft = true;
        }

        if (isEdgeDetecting && character.isOnFloor())
        {

            Vector2 bottomLeft = boxCollider2D.bounds.min;
            Vector2 bottomRight = boxCollider2D.bounds.min;
            bottomRight.x += (boxCollider2D.bounds.extents.x * 2);

			if(canWalkLeft)
			{
				Debug.DrawRay(bottomLeft, -transform.up * raycastLength, Color.red);
			}
			else
			{
				Debug.DrawRay(bottomRight, -transform.up * raycastLength, Color.red);
			}
            
            if (canWalkLeft && !Physics2D.Raycast(bottomLeft,
                -transform.up, raycastLength,
                character.charMotor2D.collisionMask))
            {
                canWalkLeft = false;
            } 
			else if (!canWalkLeft && !Physics2D.Raycast(bottomRight,
                -transform.up, raycastLength,
                character.charMotor2D.collisionMask))
            {
                canWalkLeft = true;
            }
            //and bottom right
        }

        return canWalkLeft;
    }
}
