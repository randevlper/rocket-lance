﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIBehaviourCanSeeTarget : MonoBehaviour
{
    public Character character;
    public Collider2D target;
    public bool canSeeTarget;
    public float FOV;
    public float viewDistance;
    public ContactFilter2D filter;

    // Update is called once per frame
    public bool GetCanSeeTarget()
    {
        //Raycast to target
        //Get angle
        RaycastHit2D[] hitsTop = { new RaycastHit2D() };
        RaycastHit2D[] hitsBottom = { new RaycastHit2D() };

        Vector3 raycastOrigin = transform.position;
        Vector3 center = (target.bounds.center - raycastOrigin).normalized;

        Vector3 extents = target.bounds.extents;

        Vector3 topDirection = target.bounds.center;
        topDirection.y += extents.y;
        topDirection = (topDirection - raycastOrigin).normalized;

        Vector3 bottomDirection = target.bounds.center;
        bottomDirection.y -= extents.y;
        bottomDirection = (bottomDirection - raycastOrigin).normalized;

        float angleToTop = Vector3.Angle(transform.right * character.facingDirection, topDirection);
        float angleToBottom = Vector3.Angle(transform.right * character.facingDirection, bottomDirection);

        canSeeTarget = false;

        if (angleToTop < FOV)
        {
            int numHitsTop = Physics2D.Raycast(raycastOrigin, topDirection, filter, hitsTop, viewDistance);
            Debug.DrawRay(raycastOrigin, topDirection * viewDistance, Color.blue);

            if (numHitsTop > 0)
            {
                for (int i = 0; i < numHitsTop; i++)
                {
                    Debug.DrawLine(raycastOrigin, hitsTop[i].point, Color.yellow);
                    if (hitsTop[i].collider == target)
                    {
                        canSeeTarget = true;
                    }
                }
            }
        }

        if (angleToBottom < FOV)
        {
            int numHitsBottom = Physics2D.Raycast(raycastOrigin, bottomDirection, filter, hitsBottom, viewDistance);
            Debug.DrawRay(raycastOrigin, bottomDirection * viewDistance, Color.blue);

            if (numHitsBottom > 0)
            {
                for (int i = 0; i < numHitsBottom; i++)
                {
                    Debug.DrawLine(raycastOrigin, hitsBottom[i].point, Color.yellow);
                    if (hitsBottom[i].collider == target)
                    {
                        canSeeTarget = true;
                    }
                }
            }
        }

        return canSeeTarget;
    }
}
