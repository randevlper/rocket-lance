﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Gold;

public class EnragedFan : EnemyAI
{
    protected override void Start()
    {
        base.Start();
        stateMachine.Add(new StateMachineAction(Charging, OnChargeEnter, OnChargeExit), "Charging");
        stateMachine.Add(new StateMachineAction(Patroling), "Patroling");
        stateMachine.Start("Patroling");
    }

    //AI below here
    void OnChargeEnter()
    {
        character.moveSpeed = sprintSpeed;
    }

    void Charging()
    {
        Vector2 dir =
            (target.transform.position - transform.position).normalized;
        float dirX = Mathf.Sign(dir.x);
        character.SetDirectionalInput(dirX, 0);

        if (!canSeeTarget.GetCanSeeTarget())
        {
            stateMachine.ChangeState("Patroling");
        }
    }

    void OnChargeExit()
    {
        character.moveSpeed = walkSpeed;
    }

    void Patroling()
    {
        if (canMoveLeft.GetCanWalkLeft())
        {
            character.SetDirectionalInput(-1, 0);
        }
        else
        {
            character.SetDirectionalInput(1, 0);
        }

        if (canSeeTarget.GetCanSeeTarget())
        {
            stateMachine.ChangeState("Charging");
        }
    }
}