﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Gold;

public class BuffFan : EnemyAI
{
    [SerializeField] private MeleeController meleeController;
    [SerializeField] private Area2D swingZone;
    bool canHitTarget;
    void onEnter(Collider2D collider)
    {
        if (collider == canSeeTarget.target)
        {
            canHitTarget = true;
        }
    }

    void onExit(Collider2D collider)
    {
        if (collider == canSeeTarget.target)
        {
            canHitTarget = false;
        }
    }

    // Use this for initialization
    protected override void Start()
    {
        base.Start();
        swingZone.onBodyEnter += onEnter;
        swingZone.onBodyExit += onExit;

        stateMachine.Add(new StateMachineAction(Patrol,OnPatrol), "Patroling");
        stateMachine.Add(new StateMachineAction(Chase), "Chasing");
        stateMachine.Add(new StateMachineAction(Swing, OnSwing), "Swinging");
        stateMachine.Start("Patroling");

    }

    void OnPatrol()
    {
        character.SetDirectionalInput(0, 0);
    }

    void Patrol()
    {
        if (canSeeTarget.GetCanSeeTarget())
        {
            stateMachine.ChangeState("Chasing");
        }
    }

    void Chase()
    {
        if (canSeeTarget.GetCanSeeTarget())
        {
            //Go to target
            Vector2 dir = (target.transform.position - transform.position).normalized;
            float dirX = Mathf.Sign(dir.x);
            character.SetDirectionalInput(dirX, 0);

            if (canHitTarget)
            {
                stateMachine.ChangeState("Swinging");
            }
        }
        else
        {
            stateMachine.ChangeState("Patroling");
        }
    }

    void OnSwing()
    {
        meleeController.Attack();
        character.SetDirectionalInput(0, 0);
    }

    void Swing()
    {
        if (!meleeController.IsAttacking)
        {
            stateMachine.ChangeState("Chasing");
        }
    }


}