﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/* 
PlayerController - Idea of a player
PlayerState - State of the player
ACharacter - The physical body of a character
 */

public class PlayerController : MonoBehaviour {
    public static PlayerController instance;

    //This should be for all characters
    [SerializeField] private CharacterSpriteManager characterSpriteManager;
    public CharacterHealth characterHealth;
    public Character character;
    public BoxCollider2D hitbox;

    public float immunityLength;
    [SerializeField] private int flashTimes;
    public Vector2 knockback;


    [Header("Wwise")]
    public AK.Wwise.Event damageSound;

    // Use this for initialization
    void Awake () {
        if (instance == null) {
            instance = this;
            DontDestroyOnLoad (gameObject);
            characterHealth.OnDeath += Death;
            characterHealth.OnDamage += Damage;
        } else if (instance != this) {
            Destroy (gameObject);
        }
    }

    private void Update () {
        characterSpriteManager.SetSpriteDirection (character.facingDirection);
    }

    void Death () {
        //Kill player and respawn at starting area
        ServiceManager.instance.levelManager.Respawn ();
        Respawn ();
    }

    public void Respawn () {
        characterHealth.Health = characterHealth.MaxHealth;
    }

    void Damage (DamageData hit) {
        damageSound.Post(gameObject);
        characterSpriteManager.Flash (flashTimes, immunityLength / flashTimes);
        Knockback (hit.other);
        if (!hit.damageOverTime) {
            characterHealth.canTakeDamage = false;
            characterHealth.DelaySetCanTakeDamage (immunityLength, true);
        }
    }

    void Knockback (GameObject other) {
        Vector2 dir = (characterHealth.transform.position - other.transform.position).normalized;
        float facingX = Mathf.Sign (dir.x);

        character.velocity = new Vector3 (
            facingX * knockback.x,
            knockback.y,
            character.velocity.z);
    }

    private void OnDestroy () {
        if (instance == this && CameraController.instance != null) {
            Destroy (CameraController.instance.gameObject);
        }
    }

    void OnBodyEnter (Collider2D other) { }

    void OnBodyStay (Collider2D other) { }

    void OnBodyExit (Collider2D other) { }
}