﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMeleeController : MonoBehaviour {
    [Header ("Dependencies")]
    public Character character;
    [Header ("Vertical")]
    [SerializeField]
    private Area2D verticalArea2D;
    [SerializeField] private SpriteRenderer verticalSprite;

    [Header ("Horizontal")]
    [SerializeField]
    private Area2D horizontalArea2D;
    [SerializeField] private SpriteRenderer horizontalSprite;

    public DamageData hit;
    public float attackLength;

    bool isAttacking;

    private void Awake () {
        verticalArea2D.onBodyEnter = Collision;
        horizontalArea2D.onBodyEnter = Collision;

        verticalArea2D.gameObject.SetActive (false);
        horizontalArea2D.gameObject.SetActive (false);
    }

    public void Attack () {
        if (!isAttacking) {
            //This needs to have compensation for controllers
            Vector2 facing = PlayerInput.GetDirection ();
            if (facing.y != 0) {
                VerticalAttack (facing.y);
            } else if (character.facingDirection != 0) {
                HorizontalAttack (character.facingDirection);
            }
            Invoke ("ResetAttacks", attackLength);
        }
    }

    void HorizontalAttack (float x) {
        Vector3 newPos = horizontalArea2D.gameObject.transform.localPosition;
        if (x != 0) {
            horizontalArea2D.gameObject.SetActive (true);
        }

        if (x < 0) {
            horizontalSprite.flipX = true;
            newPos.x = -Mathf.Abs (newPos.x);
        } else if (x > 0) {
            horizontalSprite.flipX = false;
            newPos.x = Mathf.Abs (newPos.x);
        }
        isAttacking = true;
        horizontalArea2D.gameObject.transform.localPosition = newPos;
    }

    void VerticalAttack (float y) {
        Vector3 newPos = verticalArea2D.gameObject.transform.localPosition;
        if (y != 0) {
            verticalArea2D.gameObject.SetActive (true);
        }
        if (y < 0) {
            verticalSprite.flipY = true;
            newPos.y = -Mathf.Abs (newPos.y);
        } else if (y > 0) {
            verticalSprite.flipY = false;
            newPos.y = Mathf.Abs (newPos.y);
        }
        isAttacking = true;
        verticalArea2D.gameObject.transform.localPosition = newPos;
    }

    void ResetAttacks () {
        verticalArea2D.gameObject.SetActive (false);
        horizontalArea2D.gameObject.SetActive (false);
        isAttacking = false;
    }

    void Collision (Collider2D other) {
        IDamageable damageable = other.GetComponent<IDamageable> ();
        if (damageable != null) {
            damageable.Damage (hit);
        }
    }

}