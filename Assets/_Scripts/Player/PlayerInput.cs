﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent (typeof (Character))]
public class PlayerInput : MonoBehaviour {

	Character player;
	CharacterMotor2D playerMotor;
	public MeleeController meleeController;
	public ContactFilter2D contactFilter;

	// Use this for initialization
	void Start () {
		player = GetComponent<Character> ();
		playerMotor = GetComponent<CharacterMotor2D> ();

		InputManager.AddButton("Jump", KeyCode.Space);
		InputManager.AddButton("Fire1", KeyCode.Z);
		InputManager.AddButton("Dash", KeyCode.LeftShift);
		InputManager.AddAxis("Vertical", KeyCode.DownArrow, KeyCode.UpArrow);
		InputManager.AddAxis("Horizontal", KeyCode.LeftArrow, KeyCode.RightArrow);
	}

	// Update is called once per frame
	void Update () {
		Vector2 directionalInput = GetDirection ();
		player.SetDirectionalInput (directionalInput);

		if (directionalInput.y < 0) {
			//Is there a fall through platform below the player?
			//Pull this out of here
			//Floor detection script
			RaycastHit2D[] hits = { new RaycastHit2D () };
			Physics2D.Raycast (
				transform.position, -transform.up,
				contactFilter, hits, 0.1f);

			bool isFallable = false;
			foreach (var item in hits) {
				if (item.collider != null) {
					if (item.collider.tag == "Through") {
						isFallable = true;
					}
				}
			}

			if (InputManager.GetButtonDown ("Jump") && isFallable) {
				playerMotor.collisions.fallingThroughPlatform = true;
				Invoke ("FallingThroughPlatformFalse", 0.5f);
			}
		}

		if (!playerMotor.FallingThroughPlatform ()) {
			if (InputManager.GetButtonDown ("Jump")) {
				player.OnJumpInputDown ();
			} else if (InputManager.GetButtonUp ("Jump")) {
				player.OnJumpInputUp ();
			}
		}

		//Dashing
		if (InputManager.GetButtonDown ("Dash")) {
			player.OnDashInputDown ();
		}

		if (InputManager.GetButtonDown ("Fire1")) {
			meleeController.Attack ();
		}
	}

	void FallingThroughPlatformFalse () {
		playerMotor.collisions.fallingThroughPlatform = false;
	}

	public static Vector2 GetDirection () {
		return new Vector2 (InputManager.GetAxisRaw ("Horizontal"), InputManager.GetAxisRaw ("Vertical"));
	}
}