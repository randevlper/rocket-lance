﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct DamageData {
	public float damage;
	public bool knockback;
	public bool killInstantly;
	public bool damageOverTime;
	public GameObject other;

	public DamageData (float damage, GameObject you, bool knockback = false) {
		this.damage = damage;
		this.knockback = knockback;
		killInstantly = false;
		damageOverTime = false;
		other = you;
	}

	public DamageData(DamageData other)
	{
		damage = other.damage;
		knockback = other.knockback;
		killInstantly = other.killInstantly;
		damageOverTime = other.damageOverTime;
		this.other = other.other;
	}
}

public interface IDamageable {
	void Damage (DamageData hit);
}