﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent (typeof (Collider2D))]
public class DamageArea : MonoBehaviour {

	LayerMask mask;
	Collider2D col2D;

	public DamageData hit;
	public bool isConstantDamage = true;

	void Awake () {
		mask = LayerMask.GetMask ("Player", "Enemy");
		col2D = GetComponent<Collider2D> ();
	}

	// Update is called once per frame
	void Update () {
		if (col2D is BoxCollider2D) {
			BoxOverlap ();
		}
	}

	Vector2 pointA;
	Vector2 pointB;
	Collider2D[] BoxOverlap () {
		Collider2D[] retval = null;

		pointA = new Vector2 (
			col2D.bounds.center.x - col2D.bounds.extents.x,
			col2D.bounds.center.y + col2D.bounds.extents.y);

		pointB = new Vector2 (
			col2D.bounds.center.x + col2D.bounds.extents.x,
			col2D.bounds.center.y - col2D.bounds.extents.y);

		Collider2D[] hits = Physics2D.OverlapAreaAll (pointA, pointB, mask);
		for (int i = 0; i < hits.Length; i++) {
			//If this is true, make sure its the player then start level load
			//Needs next scene name and spawn name
			IDamageable damageable = hits[i].GetComponent<IDamageable>();
			if(damageable != null)
			{
				if(isConstantDamage)
				{
					DamageData smoothedHit = new DamageData(hit);
					smoothedHit.damage *= Time.deltaTime;
					damageable.Damage(smoothedHit);
				}
			}
		}

		return retval;
	}

	private void OnDrawGizmos () {
		if (col2D == null) {
			col2D = GetComponent<BoxCollider2D> ();
		}
		Gizmos.color = Color.red;
		Gizmos.DrawCube (col2D.bounds.center, col2D.bounds.size);
	}
}