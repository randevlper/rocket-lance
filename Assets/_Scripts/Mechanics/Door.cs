﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


//Make use of Area2D
public class Door : MonoBehaviour
{

    // Use this for initialization
    public BoxCollider2D boxCollider2D;
	public Area2D area2D;
	
    Color gizmoColor = new Color(0, 1, 0, 0.5f);

    public string GoToScene;
    public string GoToDoor;
    public bool IsFloor;

    //static bool isTransitioning;

    public Vector2 SpawnPosition
    {
        get { return boxCollider2D.bounds.center; }
    }

    void Awake()
    {
		//This done on runtime
		area2D.onBodyEnter += CheckForPlayer;
    }

    private void CheckForPlayer(Collider2D collider)
    {
        if (collider.tag == "Player")
        {
            //if (!isTransitioning) {
            //Debug.Log ("Transition");
            //isTransitioning = true;
            ServiceManager.instance.levelManager.LoadNextScene(GoToScene, GoToDoor, IsFloor);
            //}

        }
    }

    private void OnDrawGizmos()
    {
        if (boxCollider2D == null)
        {
            boxCollider2D = GetComponent<BoxCollider2D>();
        }
        Gizmos.color = gizmoColor;
        Gizmos.DrawCube(boxCollider2D.bounds.center, boxCollider2D.size);
    }

}