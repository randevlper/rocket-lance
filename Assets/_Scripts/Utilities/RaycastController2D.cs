﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent (typeof (BoxCollider2D))]
public class RaycastController2D : MonoBehaviour {

	public LayerMask collisionMask;
	public const float skinWidth = 0.015f;
	const float dstBetweenRays = 0.25f;

	protected int horizontalRayCount = 4;
	protected int verticalRayCount = 4;

	protected float horizontalRaySpacing;
	protected float verticalRaySpacing;

	protected new BoxCollider2D collider;
	protected RaycastOrigins raycastOrigins;

	protected virtual void Awake () {
		collider = GetComponent<BoxCollider2D>();
		CalculateRaySpacing();

	}
	protected virtual void Start()
	{
		CalculateRaySpacing();
	}

	//This must be done every update for moving objects
	protected void UpdateRaycastOrigins()
	{
		Bounds bounds = collider.bounds;
		bounds.Expand(skinWidth*-2);

		raycastOrigins.bottomLeft = new Vector2(bounds.min.x, bounds.min.y);
		raycastOrigins.bottomRight = new Vector2(bounds.max.x, bounds.min.y);
		raycastOrigins.topLeft = new Vector2(bounds.min.x, bounds.max.y);
		raycastOrigins.topRight = new Vector2(bounds.max.x, bounds.max.y);
	}

	protected void CalculateRaySpacing()
	{
		Bounds bounds = collider.bounds;
		bounds.Expand(skinWidth*-2);

		float boundsWidth = bounds.size.x;
		float boundsHeight = bounds.size.y;

		horizontalRayCount = Mathf.RoundToInt(boundsHeight/dstBetweenRays);
		verticalRayCount = Mathf.RoundToInt(boundsWidth/dstBetweenRays);

		//horizontalRayCount = Mathf.Clamp(horizontalRayCount, 2 , int.MaxValue);
		//verticalRayCount = Mathf.Clamp(verticalRayCount, 2 , int.MaxValue);
		
		horizontalRaySpacing = bounds.size.y / (horizontalRayCount - 1);
		verticalRaySpacing = bounds.size.x / (verticalRayCount - 1);
	}

	public struct RaycastOrigins
	{
		public Vector2 topLeft, topRight;
		public Vector2 bottomLeft, bottomRight;
	};
}
