﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour {
    public float hitDamage;
    public float immunityLength;

    [SerializeField] private int flashTimes;
    [SerializeField] private Area2D hitbox;
    [SerializeField] private CharacterSpriteManager characterSpriteManager;
    [SerializeField] private CharacterHealth characterHealth;
    public Character character;
    public Vector2 knockback;


    [Header("Wwise")]
    public AK.Wwise.Event deathSound;

    // Use this for initialization
    void Awake () {
        hitbox.onBodyEnter += OnBodyEnter;
        hitbox.onBodyStay += OnBodyStay;
        hitbox.onBodyExit += OnBodyExit;
    }

    private void Start () {
        characterHealth.OnDamage += Damage;
        characterHealth.OnDeath += Death;
    }

    // Update is called once per frame
    void Update () {
        characterSpriteManager.SetSpriteDirection (character.facingDirection);
        //If See player attack BehaviourModule
        //
    }

    void Damage (DamageData hit) {
        characterSpriteManager.Flash (flashTimes, immunityLength / flashTimes);
        Knockback (hit.other);
        if (!hit.damageOverTime) {
            characterHealth.canTakeDamage = false;
            characterHealth.DelaySetCanTakeDamage (immunityLength, true);
        }
    }

    void Knockback (GameObject other) {
        Vector2 dir = (characterHealth.transform.position - other.transform.position).normalized;
        float facingX = Mathf.Sign (dir.x);

        character.velocity = new Vector3 (
            facingX * knockback.x,
            knockback.y,
            character.velocity.z);
    }

    void Death () {
        deathSound.Post(gameObject);
        Destroy (gameObject);
    }

    void OnBodyEnter (Collider2D other) { }

    void OnBodyStay (Collider2D other) {
        IDamageable damage = other.GetComponent<IDamageable> ();
        if (damage != null) {
            damage.Damage (new DamageData (hitDamage, gameObject));
        }
    }

    void OnBodyExit (Collider2D other) {

    }
}