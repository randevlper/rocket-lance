﻿/*
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterMotor : MonoBehaviour {

	public Character character;

	int levelLayerMask = 1 << 8;

	public enum eCharacterMotor
	{
		MOVE_RIGHT,
		MOVE_LEFT
	}
	//Returns if the player is jumpin
	public bool canJump = true;

	//Jump Variables

	private int _maxJumps = 2;
	private int _numJumps = 0;

	[SerializeField]
	private bool _isGrounded;

	public bool isGrounded
	{
		get 
		{
			return _isGrounded;
		}
	}
	public int isGroundedInt
	{
		get 
		{
			if (_isGrounded)
			{
				return 1;
			}
			else
			{
				return 0;
			}
		}
	}

	/// <summary>
	/// Update is called every frame, if the MonoBehaviour is enabled.
	/// </summary>
	void Update()
	{
		float raycastDistance = 0.05f;

		RaycastHit2D hitCenter 
			= Physics2D.Raycast(
				character.transform.position,
				Vector2.down,raycastDistance,
				levelLayerMask
				);
		RaycastHit2D hitRight 
			= Physics2D.Raycast(
				character.transform.position + new Vector3(0.25f,0,0),
				Vector2.down,raycastDistance,
				levelLayerMask
				);
		RaycastHit2D hitLeft 
			= Physics2D.Raycast(
				character.transform.position + new Vector3(-0.25f,0,0),
				Vector2.down,raycastDistance,
				levelLayerMask
				);

		//Debug.Log(Physics2D.Raycast(character.transform.position,Vector2.down,0.1f,8).point);

		Debug.DrawRay(character.transform.position + new Vector3(0.25f,0,0),Vector2.down * raycastDistance);
		Debug.DrawRay(character.transform.position + new Vector3(-0.25f,0,0),Vector2.down * raycastDistance);
		Debug.DrawRay(character.transform.position,Vector2.down * raycastDistance);


		if(hitCenter.collider != null || hitLeft.collider != null || hitRight.collider != null)
		{
			_isGrounded = true;
			_numJumps = 0;
		}
		else
		{
			_isGrounded = false;
		}
	}

	//TODO Add movement
	public bool Jump()
	{
		if(_numJumps > _maxJumps)
		{
			canJump = false;
		}
		else 
		{
			canJump = true;
		}

		if (canJump && isGrounded)
		{
			character.charRigidbody.AddForce(Vector2.up * 250);
			_numJumps++;
			return true;
		}
		return false;
	}

	//Speed
	//Sprint Speed

	//Move right

	bool canMove = true;
	float speed = 250;


	[SerializeField]
	bool canMoveRight;

	[SerializeField]
	bool canMoveLeft;
	public bool Move(eCharacterMotor state)
	{
		canMoveRight = true;
		canMoveLeft = true;
		float rayDistance = 0.4f;

		switch (state)
		{
			case eCharacterMotor.MOVE_RIGHT:

			if(!isGrounded)
			{
				Debug.DrawRay(character.transform.position + new Vector3(0,0.1f,0),Vector2.right * rayDistance);
				Debug.DrawRay(character.transform.position + new Vector3(0,1f,0),Vector2.right * rayDistance);

				RaycastHit2D hitRightBottom 
					= Physics2D.Raycast(
						character.transform.position + new Vector3(0,0.1f,0),
						Vector2.right,
						rayDistance,levelLayerMask);

				
				RaycastHit2D hitRightTop 
					= Physics2D.Raycast(
						character.transform.position + new Vector3(0,1,0),
						Vector2.right,
						rayDistance,levelLayerMask);


				if(hitRightBottom.collider != null || hitRightTop.collider != null)
				{
					//Debug.Log("Hit Right"); 
					canMoveRight = false;
					
				}
			
			}
			
			if (canMoveRight)
			{
				
				RaycastHit2D movementHit = Physics2D.Raycast(
					character.transform.position + new Vector3(0.25f,0f,0),
					Vector2.down,
					0.05f,
					levelLayerMask
				);
				Debug.DrawRay(
					character.transform.position + new Vector3(0.25f,0f,0),
					Vector2.down * 0.05f,
					Color.red
				);

				Vector2 perpendicular =  
					MathG.DegreeToVector(MathG.VectorToDegrees(movementHit.normal) - 90, 1);
				if(movementHit.collider == null)
				{
					perpendicular = new Vector2(1,0);
				}
				Debug.DrawRay(character.transform.position,perpendicular);
				

				character.charRigidbody.velocity 
					= new Vector2(
						speed * Time.deltaTime, 
						character.charRigidbody.velocity.y + perpendicular.y * 1.5f
						);

			}			
			break;
			case eCharacterMotor.MOVE_LEFT:

			if(!isGrounded)
			{
				Debug.DrawRay(character.transform.position + new Vector3(0,0.1f,0),Vector2.left * rayDistance);
				Debug.DrawRay(character.transform.position + new Vector3(0,1f,0),Vector2.left * rayDistance);

				RaycastHit2D hitLeftBottom 
					= Physics2D.Raycast(
						character.transform.position + new Vector3(0,0.1f,0),
						Vector2.left,
						rayDistance,levelLayerMask);

				RaycastHit2D hitLeftTop 
					= Physics2D.Raycast(
						character.transform.position + new Vector3(0,1,0),
						Vector2.left,
						rayDistance,levelLayerMask);

				if(hitLeftBottom.collider != null || hitLeftTop.collider != null)
				{
					//Debug.Log("Hit Left");
					canMoveLeft = false;
				}
				
			}

			if (canMoveLeft)
			{
				
				RaycastHit2D movementHit = Physics2D.Raycast(
					character.transform.position + new Vector3(-0.25f,0f,0),
					Vector2.down,
					0.05f,
					levelLayerMask
				);

				Debug.DrawRay(
					character.transform.position + new Vector3(-0.25f,0f,0),
					Vector2.down * 0.05f,
					Color.red
				);

				Vector2 perpendicular =  
					MathG.DegreeToVector(MathG.VectorToDegrees(movementHit.normal) + 90, 1);
				if(movementHit.collider == null)
				{
					perpendicular = new Vector2(-1,0);
				}
				Debug.DrawRay(character.transform.position,perpendicular);
				

				character.charRigidbody.velocity 
					= new Vector2(
						speed * Time.deltaTime * perpendicular.x, 
						character.charRigidbody.velocity.y + perpendicular.y * 1.5f 
						);

			}
			
			break;
		}
		return true;
	}
}
*/