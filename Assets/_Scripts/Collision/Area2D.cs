﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//For now use only BoxCollider2D
public class Area2D : MonoBehaviour
{
    //ComputePenetration
    //IsTouching
    public List<Collider2D> collidingBodies;
    private Collider2D[] _newCollidingBodies;

    public Collider2D col2D;
    public ContactFilter2D mask;
    //Call these whenever a collider overlaps this collider
    public delegate void AreaCollision(Collider2D other);
    public AreaCollision onBodyEnter;
    public AreaCollision onBodyStay;
    public AreaCollision onBodyExit;

    private void Awake()
    {
        collidingBodies = new List<Collider2D>();
        _newCollidingBodies = new Collider2D[8];
        if (col2D == null)
        {
            col2D = GetComponent<Collider2D>();
        }
    }


    
    private void FixedUpdate()
    {
        int numNewCollidingBodies = col2D.OverlapCollider(mask, _newCollidingBodies);
        //Loop through and check
        //if its still in there, call OnBodyStay,
        //If its not in there, call OnBodyExit and null out that slot
        //If its not in the collidingBodies call OnBodyEnter

        for (int n = 0; n < numNewCollidingBodies; n++)
        {
            bool isAlreadyColliding = false;
			//Check if there are any new colliding bodies and call onBodyStay
            for (int c = 0; c < collidingBodies.Count; c++)
            {
                if (_newCollidingBodies[n] == collidingBodies[c])
                {
                    //Debug.Log(newCollidingBodies[n].name + " stays inside of " + gameObject.name);
                    isAlreadyColliding = true;

                    if (onBodyStay != null)
                    {
                        onBodyStay(_newCollidingBodies[n]);
                    }
                    else
                    {
                        //Debug.LogWarning("onBodyStay does not have any subscribed methods!", gameObject);
                    }
                }
            }

            //Add the bodies and call onBodyEnter
            if (!isAlreadyColliding)
            {
                //Debug.Log(newCollidingBodies[n].name + " enters inside of " + gameObject.name);

                if (onBodyEnter != null)
                {
                    onBodyEnter(_newCollidingBodies[n]);
                }
                else
                {
                    //Debug.LogWarning("onBodyEnter does not have any subscribed methods!", gameObject);
                }
                collidingBodies.Add(_newCollidingBodies[n]);
            }


        }

        //Check if all are colliding if not remove from the list and call onBodyExit
        for (int c = 0; c < collidingBodies.Count; c++)
        {
            bool isTouching = false;
            for (int n = 0; n < numNewCollidingBodies; n++)
            {
				if(_newCollidingBodies[n] == collidingBodies[c])
				{
					isTouching = true;
				}
            }

            if (!isTouching)
            {
                //Debug.Log(collidingBodies[c].name + " exits " + gameObject.name);

                if (onBodyExit != null)
                {
                    onBodyExit(collidingBodies[c]);
                }
                else
                {
                    //Debug.LogWarning("onBodyExit does not have any subscribed methods!", gameObject);
                }


                collidingBodies.RemoveAt(c);
            }
        }
	}

    private void OnEnable()
    {
        collidingBodies.Clear();
    }
}