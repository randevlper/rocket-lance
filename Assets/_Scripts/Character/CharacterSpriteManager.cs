﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterSpriteManager : MonoBehaviour {
	[SerializeField] private SpriteRenderer spriteRenderer;

	//Flash Invoke
	//Number of flashes
	//How long to flash

	int   flashNum;
	float flashLength;
	bool isFlashing;

	private void Awake()
	{
		//spriteRenderer.material = new Material(spriteFlash);
	}

	public void Flash(int num, float length)
	{
		if(!isFlashing)
		{
			flashNum = num;
			flashLength = length;
			StartCoroutine(Flash());
		}
	}

	public void SetSpriteDirection(int facingX)
	{
		if(facingX < 0)
		{
			spriteRenderer.flipX = false;
		}
		else if (facingX > 0)
		{
			spriteRenderer.flipX = true;
		}
	}

	IEnumerator Flash()
	{
		isFlashing = true;
		bool isFlash = false;
		for(int i = 0; i < flashNum * 2; i++)
		{
			
			if(isFlash)
			{
				//Make 0
				spriteRenderer.material.SetFloat("_FlashAmount", 0);
				isFlash = false;
			}
			else
			{
				//Make 1
				spriteRenderer.material.SetFloat("_FlashAmount", 1);
				isFlash = true;
			}

			yield return new WaitForSeconds(flashLength);
		}
		spriteRenderer.material.SetFloat("_FlashAmount", 0);
		isFlashing = false;
	}

	private void OnDisable()
	{
		isFlashing = false;
		StopCoroutine(Flash());
	}
}
