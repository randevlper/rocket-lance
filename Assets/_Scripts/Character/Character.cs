﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent (typeof (CharacterMotor2D))]
public class Character : MonoBehaviour {

    [Header ("Jump")]
    public float maxJumpHeight = 4;
    public float minJumpHeight = 1;
    public float timeToJumpApex = 0.4f;
    [SerializeField] float accelerationTimeAirborne = 0.2f;
    [SerializeField] float accelerationTimeGrounded = 0.1f;
    [SerializeField] float accelerationTimeGroundedDashing = 0.0f;
    bool lastFrameInAir;

    [Header ("Wall")]
    public Vector2 wallJumpClimb;
    public Vector2 wallJumpOff;
    public Vector2 wallJumpLeap;
    public float wallSlideSpeedMax = 3;
    public float wallStickTime = .1f;
    float timeToWallUnstick;

    [Header ("Speed")]
    public float moveSpeed = 6;
    //float crouchSpeed = 3;

    [Header ("Dash")]
    public float dashDistance = 4;
    public float dashTime = 0.1f;
    public float dashCooldown = 1.0f;
    float dashSpeed;
    float dashTimer;
    float dashCooldownTimer = 0;
    bool isDashing = false;

    float gravity;
    float minJumpVelocity;
    float maxJumpVelocity;
    public Vector3 velocity;
    float velocityXSmoothing;

    [Header ("Info")]
    //CharacterData charData;
    public CharacterMotor2D charMotor2D;
    public int facingDirection = 1;

    Vector2 directionalInput;
    int wallDirX;
    bool wallSliding;

    //Flags
    [Header ("Flags")]
    public bool canWallSlide;
    public bool canWallJump;
    public bool canDash;
    public bool isKnockbacked;

    //Delgates
    public Gold.Delegates.Inform onJump;
    public Gold.Delegates.Inform onLand;


    // Use this for initialization
    void Awake () {
        //charData = GetComponent<CharacterData> ();
        charMotor2D = GetComponent<CharacterMotor2D> ();

        gravity = -(2 * maxJumpHeight) / Mathf.Pow (timeToJumpApex, 2);
        maxJumpVelocity = Mathf.Abs (gravity) * timeToJumpApex;
        minJumpVelocity = Mathf.Sqrt (2 * Mathf.Abs (gravity) * minJumpHeight);
        //print("Gravity: " + gravity + " Jump Velocity" + maxJumpVelocity);

        dashSpeed = dashDistance / dashTime;

        onJump += Empty;
        onLand += Empty;
    }

    void Empty(){}

    // Update is called once per frame
    void Update () {

        CalculateVelocity ();
        HandleWallsliding ();

        if (isDashing) {
            Dash ();
        } else {
            if (directionalInput.x != 0) {
                //print("directionalInput.x: " + directionalInput.x);
                facingDirection = (int) Mathf.Sign (directionalInput.x);
            }
        }

        //Check if landed
        if(!lastFrameInAir && charMotor2D.collisions.below)
        {
            onLand();
        }
        lastFrameInAir = charMotor2D.collisions.below;

        charMotor2D.Move (velocity * Time.deltaTime, directionalInput);
        if (charMotor2D.collisions.above || charMotor2D.collisions.below) {
            if (charMotor2D.collisions.slidingDownMaxSlope) {
                velocity.y = charMotor2D.collisions.slopeNormal.y *
                    -gravity * Time.deltaTime;
            } else {
                velocity.y = 0;
            }
        }
        DashCooldown ();

    }

    void Dash () {
        if (dashTimer > dashTime) {
            velocity.x = 0;
            isDashing = false;
            dashTimer = 0;
            dashCooldownTimer = 0;
        } else {
            velocity.x = facingDirection * dashSpeed;
            dashTimer += Time.deltaTime;
        }

    }
    void DashCooldown () {
        if (!(dashCooldownTimer > dashTime)) {
            dashCooldownTimer += Time.deltaTime;
        }
    }

    public void SetDirectionalInput (Vector2 input) {
        directionalInput = input;
    }

    public void SetDirectionalInput (float x, float y) {
        directionalInput.x = x;
        directionalInput.y = y;
    }

    public void OnDashInputDown () {
        if (charMotor2D.collisions.below && !wallSliding && canDash &&
            !isDashing && dashCooldownTimer > dashTime) {
            isDashing = true;
        }

    }

    public void OnJumpInputDown () {
        if (wallSliding && canWallJump) {
            if (wallDirX == directionalInput.x) {
                velocity.x = -wallDirX * wallJumpClimb.x;
                velocity.y = wallJumpClimb.y;
                onJump();
            } else if (directionalInput.x == 0) {
                velocity.x = -wallDirX * wallJumpOff.x;
                velocity.y = wallJumpOff.y;
                onJump();
            } else {
                velocity.x = -wallDirX * wallJumpLeap.x;
                velocity.y = wallJumpLeap.y;
                onJump();
            }
        }
        if (charMotor2D.collisions.below && !isDashing &&
            !charMotor2D.FallingThroughPlatform ()) {
            if (charMotor2D.collisions.slidingDownMaxSlope) {
                //Not jumping against max slope
                if (directionalInput.x != -Mathf.Sign (charMotor2D.collisions.slopeNormal.x)) {
                    velocity.y = maxJumpVelocity * charMotor2D.collisions.slopeNormal.y;
                    velocity.x = maxJumpVelocity * charMotor2D.collisions.slopeNormal.x;
                    onJump();
                }
            } else {
                velocity.y = maxJumpVelocity;
                onJump();
            }
        }
    }

    public void OnJumpInputUp () {
        if (velocity.y > minJumpVelocity) {
            velocity.y = minJumpVelocity;
        }
    }

    void CalculateVelocity () {
        float targetVelocityX = directionalInput.x * moveSpeed;
        float accelTime = (charMotor2D.collisions.below) ?
            accelerationTimeGrounded : accelerationTimeAirborne;

        if (isDashing) {
            accelTime = accelerationTimeGroundedDashing;
        }

        velocity.x = Mathf.SmoothDamp (
            velocity.x,
            targetVelocityX,
            ref velocityXSmoothing,
            accelTime);
        velocity.y += gravity * Time.deltaTime;
    }

    void HandleWallsliding () {
        wallDirX = (charMotor2D.collisions.left) ? -1 : 1;
        wallSliding = false;
        if ((charMotor2D.collisions.left || charMotor2D.collisions.right) &&
            !charMotor2D.collisions.below && velocity.y < 0 && !isDashing && canWallSlide) {
            wallSliding = true;
            if (velocity.y < -wallSlideSpeedMax) {
                velocity.y = -wallSlideSpeedMax;
            }

            if (timeToWallUnstick > 0) {
                velocityXSmoothing = 0;
                velocity.x = 0;

                if (directionalInput.x != wallDirX && directionalInput.x != 0) {
                    timeToWallUnstick -= Time.deltaTime;
                } else {
                    timeToWallUnstick = wallStickTime;
                }
            } else {
                timeToWallUnstick = wallStickTime;
            }

        }
    }

    public bool isOnFloor () {
        return charMotor2D.collisions.below;
    }

    public bool isOnCeiling () {
        return charMotor2D.collisions.above;
    }

    public bool isOnRightWall () {
        return charMotor2D.collisions.right;
    }

    public bool isOnLeftWall () {
        return charMotor2D.collisions.left;
    }
}