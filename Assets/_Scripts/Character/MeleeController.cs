﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeleeController : MonoBehaviour {
	[Header ("Dependencies")]
	public Character character;

	[Header ("Vertical")]
	[SerializeField] private Area2D verticalArea2D;
	[SerializeField] private SpriteRenderer verticalSprite;
	public AK.Wwise.Event verticalSound;

	[Header ("Horizontal")]
	[SerializeField] private Area2D horizontalArea2D;
	[SerializeField] private SpriteRenderer horizontalSprite;
	public AK.Wwise.Event horizontalSound;

	public DamageData hit;
	public float attackLength;

	private bool isAttacking;
	public bool IsAttacking {
		get { return isAttacking; }
	}

	private void Awake () {

		if (verticalArea2D != null) {
			verticalArea2D.onBodyEnter = Collision;
			verticalArea2D.gameObject.SetActive (false);
		}

		if (horizontalArea2D != null) {
			horizontalArea2D.onBodyEnter = Collision;
			horizontalArea2D.gameObject.SetActive (false);

		}

	}

	public void Attack () {

		if (!isAttacking) {
			//This needs to have compensation for controllers
			Vector2 facing = PlayerInput.GetDirection ();
			if (facing.y != 0 &&
				verticalArea2D != null && verticalSprite != null) {
				VerticalAttack (facing.y);
				verticalSound.Post(gameObject);
			} else if (character.facingDirection != 0 &&
				horizontalArea2D != null && horizontalSprite != null) {
				HorizontalAttack (character.facingDirection);
				horizontalSound.Post(gameObject);
			}

			Invoke ("ResetAttacks", attackLength);
		}

	}

	void ResetAttacks () {
		if (verticalArea2D != null) {
			verticalArea2D.gameObject.SetActive (false);
		}

		if (horizontalArea2D != null) {
			horizontalArea2D.gameObject.SetActive (false);
		}
		isAttacking = false;
	}

	void HorizontalAttack (float x) {
		Vector3 newPos = horizontalArea2D.gameObject.transform.localPosition;
		if (x != 0) {
			horizontalArea2D.gameObject.SetActive (true);
		}

		if (x < 0) {
			horizontalSprite.flipX = true;
			newPos.x = -Mathf.Abs (newPos.x);
		} else if (x > 0) {
			horizontalSprite.flipX = false;
			newPos.x = Mathf.Abs (newPos.x);
		}
		isAttacking = true;
		horizontalArea2D.gameObject.transform.localPosition = newPos;
	}

	void VerticalAttack (float y) {
		Vector3 newPos = verticalArea2D.gameObject.transform.localPosition;
		verticalArea2D.gameObject.SetActive (true);
		if (y < 0) {
			verticalSprite.flipY = true;
			newPos.y = -Mathf.Abs (newPos.y);
		} else if (y > 0) {
			verticalSprite.flipY = false;
			newPos.y = Mathf.Abs (newPos.y);
		}
		isAttacking = true;
		verticalArea2D.gameObject.transform.localPosition = newPos;
	}

	void Collision (Collider2D other) {
		IDamageable damageable = other.GetComponent<IDamageable> ();
		if (damageable != null) {
			damageable.Damage (hit);
		}
	}
}