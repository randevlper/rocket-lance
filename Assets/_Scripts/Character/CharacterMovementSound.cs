﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterMovementSound : MonoBehaviour {

	public Character character;
	public AK.Wwise.Event footstep;
	public AK.Wwise.Event floorLand;
	public AK.Wwise.Event jump;

	void Awake()
	{
		character.onJump += Jump;
		character.onLand += Land;
	}

	public void TriggerFootstep()
	{
		footstep.Post(gameObject);
	}

	void Jump(){
		jump.Post(gameObject);
	}

	void Land()
	{
		floorLand.Post(gameObject);
	}
}
